from datetime import datetime
from pytz import timezone
from astronomia import calendar

def main():
    '''timezone'''
    tz = timezone("Asia/Jakarta")
    '''today datetime'''
    dt = datetime.today()
    '''convert datetime to julian day'''
    jd = calendar.cal_to_jde(dt.year, dt.month, dt.day, dt.hour, dt.minute,dt.second)
    
    ''' date format for displaying yyyy-mm-dd HH:MM:SS, 
        e.g. 2014-03-12 20:05:30 WIB'''
    dateformat="%Y-%m-%d %H:%M:%S %Z"
    
    print "Datetime (Now)\t\t\t: %s" % dt.strftime(dateformat)
    print "in Julian Day\t\t\t: %f" % jd
    
    '''initialize datetime to a desired date and time, i.e. 2014-03-05 00:00:00'''
    dt = datetime(2014,3,5,0,0,0)
    '''convert datetime ke julian day'''
    jd = calendar.cal_to_jde(dt.year, dt.month, dt.day, dt.hour, dt.minute,dt.second)
    
    '''date format for displaying yyyy-mm-dd HH:MM:SS, e.g. 2014-03-12 20:05:30'''
    print "Datetime\t\t\t: %s" % dt.strftime(dateformat)
    print "in Julian Day\t\t\t: %f" % jd
    
    '''print date time with timezone info'''
    print "Datetime with Asia/Jakarta Tz\t: %s " % tz.localize(dt).strftime(dateformat)
    
    '''initialize timezone UTC'''
    tz_utc = timezone("UTC")
    
    '''convert Western Indonesia Time (WIB) to UTC time'''
    utc_dt = dt - tz.utcoffset(dt)
    print "Datetime in UTC\t\t\t: %s" % tz_utc.localize(utc_dt).strftime(dateformat)
    
    '''Convert UTC to Central Indonesia Time (WITA)'''
    tz_wita = timezone("Asia/Makassar")
    wita_dt = utc_dt + tz_wita.utcoffset(dt)
    print "Datetime in Asia/Makassar Tz\t: %s" % tz_wita.localize(wita_dt).strftime(dateformat)
    
        
if __name__ == '__main__':
    main()

    