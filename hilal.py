from pytz import timezone
from pyislamia.tool import frac_of_day,jd_to_datetime, d_to_dms
from pyislamia import moon, constants as c
from astronomia import calendar,util
from datetime import datetime


def main():
    '''timezone'''
    tz = timezone("Asia/Jakarta")
    '''today datetime'''
    dt = datetime.today()
    jd = calendar.cal_to_jde(dt.year, dt.month, dt.day, dt.hour, dt.minute,dt.second) #julian day in local_time
    jd_utc = jd - frac_of_day(tz.utcoffset(dt))
    """date time format"""
    fmt = '%Y-%m-%d'
    hour_fmt ='%H:%M:%S %Z'
    full_fmt = fmt + " " + hour_fmt
    
    """today datetime"""
    print "Date and Time\t: %s" %  tz.localize(dt).strftime(full_fmt)
    
    age_of_moon = moon.ageOfMoonInDays(jd_utc)

    '''last new moon'''
    jd_new_moon = moon.newMoon(
                               jd - age_of_moon,
                               tz.utcoffset(dt)
                               )
    
    '''Observer Latitude and longitude in [degree, minute, second]'''
    dms_lon =[107, 37, 0]
    dms_lat =[-6, 57, 0]
    
    ''' date format for displaying yyyy-mm-dd HH:MM:SS, 
        e.g. 2014-03-12 20:05:30 WIB'''
    dateformat="%Y-%m-%d %H:%M:%S %Z"
    
    
    '''Longitude 107 37' converted to radian'''
    obs_lon = util.d_to_r(util.dms_to_d(dms_lon[0],dms_lon[1],dms_lon[2])) 
    '''Latitude -6 57' converted to radian'''
    obs_lat = util.d_to_r(util.dms_to_d(dms_lat[0],dms_lat[1],dms_lat[2])) 
    '''elevation'''
    obs_elevation = 10
    
    
    print u"Observer's position\t: %d\xb0%d'%d\" %d\xb0%d'%d\"" % (dms_lon[0],dms_lon[1],dms_lon[2],dms_lat[0],dms_lat[1],dms_lat[2])
    print u"Observer's elevation\t: %d m (above sea level)" % (obs_elevation)
   
  
    
    
     
    [jd_sun_set, jd_moon_set, h1, elongation, age_of_moon ] = moon.hilalInfo(jd_new_moon, 
                                                                            obs_lon, 
                                                                            obs_lat,
                                                                            tz.utcoffset(dt), 
                                                                            obs_elevation)
   
    h1 = util.r_to_d(h1)
    dms_h1= d_to_dms(h1)
    
    elongation = util.r_to_d(elongation)
    dms_elongation = d_to_dms(elongation)
    
    hms = calendar.fday_to_hms(age_of_moon)
#     
    print "=======================Hilal of current month======================="
    print u"New Moon\t: %s" % tz.localize(jd_to_datetime(jd_new_moon)).strftime(full_fmt)
    print u"Sun Set\t\t: %s" % tz.localize(jd_to_datetime(jd_sun_set)).strftime(full_fmt)
    print u"Moon Set\t: %s" % tz.localize(jd_to_datetime(jd_moon_set)).strftime(full_fmt)
    print u"Hilal altitude\t: %d\xb0%d'%d\"" % (dms_h1[0], dms_h1[1], dms_h1[2])
    print u"Elongation\t: %d\xb0%d'%d\"" % (dms_elongation[0], dms_elongation[1], dms_elongation[2])
    print u"Age of Moon\t: %d hours %d minutes %d seconds" % (hms[0], hms[1],hms[2])
    
    
    
    print "======================Hilal of next month==========================="
    '''Next new moon'''
    jd_new_moon = moon.newMoon(
                               jd + c.SYNODIC_MONTH,
                               tz.utcoffset(dt)
                               )
    [jd_sun_set, jd_moon_set, h1, elongation, age_of_moon ] = moon.hilalInfo(jd_new_moon, 
                                                                            obs_lon, 
                                                                            obs_lat,
                                                                            tz.utcoffset(dt), 
                                                                            obs_elevation)
   
    h1 = util.r_to_d(h1)
    dms_h1= d_to_dms(h1)
    
    elongation = util.r_to_d(elongation)
    dms_elongation = d_to_dms(elongation)
    
    hms = calendar.fday_to_hms(age_of_moon)
    
    
    print u"New Moon\t: %s" % tz.localize(jd_to_datetime(jd_new_moon)).strftime(full_fmt)
    print u"Sun Set\t\t: %s" % tz.localize(jd_to_datetime(jd_sun_set)).strftime(full_fmt)
    print u"Moon Set\t: %s" % tz.localize(jd_to_datetime(jd_moon_set)).strftime(full_fmt)
    print u"Hilal altitude\t: %d\xb0%d'%d\"" % (dms_h1[0], dms_h1[1], dms_h1[2])
    print u"Elongation\t: %d\xb0%d'%d\"" % (dms_elongation[0], dms_elongation[1], dms_elongation[2])
    print u"Age of Moon\t: %d hours %d minutes %d seconds" % (hms[0], hms[1],hms[2])
    


if __name__ == "__main__":
    main()
   
    