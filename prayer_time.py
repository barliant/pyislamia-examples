
from datetime import datetime
from pytz import timezone
from astronomia import calendar,util
from pyislamia import solar
from pyislamia.tool import frac_of_day, jd_to_datetime, start_of_day
from pyislamia import prayer
from astronomia.calendar import fday_to_hms

def main():
    '''timezone'''
    tz = timezone("Asia/Jakarta")
    '''today datetime'''
    dt = datetime.now() #(2013,1,1)
#     dt = datetime(year=2013,month=1,day=1)
    jd = calendar.cal_to_jde(dt.year, dt.month, dt.day, dt.hour, dt.minute,dt.second) #julian day in local_time
  
    """date time format"""
    fmt = '%Y-%m-%d'
    hour_fmt ='%H:%M:%S %Z'
    full_fmt = fmt + " " + hour_fmt
    
    print "========calculate muslim prayer times==========="
    
    """today datetime"""
    print "Date and Time (Now)\t: %s" %  tz.localize(dt).strftime(full_fmt)
    
    
    '''Observer Latitude and longitude in [degree, minute, second]'''
    dms_lon = [107, 34, 0]
    dms_lat = [-6, 57, 0]

#     dms_lat = util.d_to_dms(-6.27780)
#     dms_lon = util.d_to_dms(106.9372)
    
    ''' date format for displaying yyyy-mm-dd HH:MM:SS, 
        e.g. 2014-03-12 20:05:30 WIB'''
    dateformat="%Y-%m-%d %H:%M:%S %Z"
    
    
    '''Longitude 107 37' converted to radian'''
    obs_lon = util.d_to_r(util.dms_to_d(dms_lon[0],dms_lon[1],dms_lon[2])) 
    '''Latitude -6 57' converted to radian'''
    obs_lat = util.d_to_r(util.dms_to_d(dms_lat[0],dms_lat[1],dms_lat[2])) 
    '''761 m above sea level'''
    elevation = 761.0
    
    print u"Observer's position\t: %d\xb0%d'%d\" %d\xb0%d'%d\"" % (dms_lon[0],dms_lon[1],dms_lon[2],dms_lat[0],dms_lat[1],dms_lat[2])
    print u"Observer's elevation\t: %f m (above sea level)" % elevation
    print "%8s %8s %8s %8s %8s %8s %8s" % ("JD at 0H","Shubuh","Syuruq","Dzuhr","Ashr","Maghib","Isya")
    times = prayer.get_prayer_times(jd, prayer.STANDARD_ANGLE["MABIMS"], prayer.ASR_JURISDICTION["SHAFII"], obs_lon, obs_lat, tz.utcoffset(dt), elevation)
    times = [times[0],] + [fday_to_hms(t) for t in times[1:]]
     
    times = [str(times[0]),] + ["{h:02d}:{m:02d}:{s:02d}".format(h=h,m=m,s=s) for h,m,s in times[1:]]
   
    print "%8s %8s %8s %8s %8s %8s %8s" % (times[0],times[1],times[2],times[3],times[4],times[5],times[6])
    
if __name__=="__main__":
    main()
    