from pytz import timezone
from pyislamia.tool import frac_of_day,jd_to_datetime
from pyislamia import moon, constants as c
from astronomia import calendar
from datetime import datetime

def main():
    '''timezone'''
    tz = timezone("Asia/Jakarta")
    '''today datetime'''
    dt = datetime.today()
    jd = calendar.cal_to_jde(dt.year, dt.month, dt.day, dt.hour, dt.minute,dt.second) #julian day in local_time
    jd_utc = jd - frac_of_day(tz.utcoffset(dt))
    """date time format"""
    fmt = '%Y-%m-%d'
    hour_fmt ='%H:%M:%S %Z'
    full_fmt = fmt + " " + hour_fmt
    
    """today datetime"""
    print "Date and Time\t: %s" %  tz.localize(dt).strftime(full_fmt)
    
    age_of_moon = moon.ageOfMoonInDays(jd_utc)

    '''last new moon'''
    jd_new_moon = moon.newMoon(
                               jd - age_of_moon,
                               tz.utcoffset(dt)
                               )
    
    '''next new moon'''
    jd_next_new_moon = moon.newMoon(jd_new_moon + c.SYNODIC_MONTH,
                                    tz.utcoffset(dt)
                                    )
     
    moon_period = jd_next_new_moon - jd_new_moon
    jd_full_moon = moon.fullMoon(jd_new_moon + moon_period/2,
                                  tz.utcoffset(dt)
                                 )
#     
    print "New Moon\t: %s" % tz.localize(jd_to_datetime(jd_new_moon)).strftime(full_fmt)
    print "Full Moon\t: %s" % tz.localize(jd_to_datetime(jd_full_moon)).strftime(full_fmt)
    print "Next New Moon\t: %s" % tz.localize(jd_to_datetime(jd_next_new_moon)).strftime(full_fmt)


if __name__ == "__main__":
    main()
   
    